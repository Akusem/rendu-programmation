## Description du programme

Ce programme vous permet de rechercher des motifs nucléotidiques ou protéiques dans deux génomes (fournis au format [Genome].gbff)
, et d'obtenir toutes les positions de ces motif ainsi que les annotations correspondantes. 

Celui-ci vous permet de choisir entre l'utilisation d'un algorithme simple pour la recherche de motif 
nucléotidique et protéique, et l'utilisation d'arbres des suffixes pour la recherche nucléotidique.

L'utilisation d'arbres des suffixes permet une recherche plus rapide de motif en contrepartie d'un temps de
précalculations de quelques minutes (pour un génome bactérien).

## Prérequis

Ce logiciel nécessite la dernière version du paquet *gtkmm* (gtkmm 3). Pour l'installer, veuillez effectuer l'opération suivante si vous êtes sous Ubuntu :

```
sudo apt install libgtkmm-3.0-dev libgstreamermm-1.0-dev
```

l'utilitaire CMake est nécéssaire pour compiler le programme :

```
sudo apt install cmake
```

## Installation du programme

Pour installer ce programme, suivez ces instructions :

```
git clone https://framagit.org/Akusem/rendu-programmation.git
cd rendu-programmation
cd Source && cmake CMakeLists.txt && make && mv Project ../ && cd ..
```

## Utilisation

#### En ligne de commandes

Entrez dans le terminal :

```
./Projet [Génome 1] [Génome 2] -[n || p] [motif] [fichier ouput] [choix algo]
```


|   -n	|   -p	|   -s	|   -h	|
|---	|---	|---	|---	|
|   Motif nucléotidique	|   Motif protéique	|   Utiliser Arbre des suffixes	|   Afficher l'aide	|


#### En mode graphique

Pour le mode graphique (**GUI**) il suffit de lancer le projet en terminal sans aucun argument comme-ceci :

```
./Projet
```

#### L'interface graphique


![Alt Text](for_ReadMe.gif)

## Documentation Fonctions

Voir [ici](Source/html/main_8cpp.html) en local pour la documentation Doxygen (Source/html/main_8cpp.html)


## Auteurs
Nathanaël Zweig & Pauline Brochet, M2 DLAD

[Nous contactez](mailto:nathanael.ZWEIG@etu.univ-amu.fr)
