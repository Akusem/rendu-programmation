#include "Gene.h"

Gene::Gene() : dnaDirection(' '), note("") {}

Gene::Gene(string m_name, long Deb, long Fin) {
    name = m_name;
    posDeb = Deb;
    posFin = Fin;
}

Gene::Gene(char m_dnaDirection, string m_note) {
    dnaDirection = m_dnaDirection;
    note = m_note;
}

Gene::Gene(string m_name, long m_posDeb, long m_posFin, char m_dnaDirection, string m_note) {
    name = m_name;
    posDeb = m_posDeb;
    posFin = m_posFin;
    dnaDirection = m_dnaDirection;
    note = m_note;
}

Gene::~Gene() {}

string Gene::getNote() {
    return note;
}

char Gene::getDnaDirection() {
    return dnaDirection;
}

void Gene::setNote(string m_note) {
    note = m_note;
}

void Gene::setDnaDirection(char m_dnaDirection) {
    dnaDirection = m_dnaDirection;
}

const string &Gene::getProteinId() const {
    return proteinId;
}

void Gene::setProteinId(const string &proteinId) {
    Gene::proteinId = proteinId;
}
