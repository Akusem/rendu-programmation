#ifndef DEF_GENE
#define DEF_GENE

#include "Objet_biologique.h"

using namespace std;

class Gene : public ObjetBiologique 
{
protected:
    string note;
    string proteinId;
    char dnaDirection;

public:

    Gene();
    Gene(string m_name, long Deb, long Fin);
    Gene(char m_dnaDirection, string m_note);
    Gene(string m_name, long m_posDeb, long m_posFin, char m_dnaDirection, string m_note);
    ~Gene();
    //string getName(pair<u_char*, ulong> const& genome);
    string getNote();
    char getDnaDirection();
    const string &getProteinId() const;
    void setNote(string m_note);
    void setDnaDirection(char m_dnaDirection);
    void setProteinId(const string &proteinId);
};

#endif