//
// Created by akusem on 15/10/18.
//

#include "Genome.h"

Genome::Genome() {
    locus = "";
    definition = "";
}

Genome::Genome(const string &locus, const string &definition, const vector<Gene> &annotation) : locus(locus), definition(definition), annotation(annotation) {}

Genome::~Genome() {}

const string &Genome::getLocus() const {
    return locus;
}

void Genome::setLocus(const string &locus) {
    Genome::locus = locus;
}

const string &Genome::getDefinition() const {
    return definition;
}

void Genome::setDefinition(const string &definition) {
    Genome::definition = definition;
}

const vector<Gene> &Genome::getAnnotation() const {
    return annotation;
}

void Genome::setAnnotation(vector<Gene> annotation) {
    Genome::annotation = annotation;
}

