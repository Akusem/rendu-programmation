//
// Created by akusem on 15/10/18.
//

#ifndef PROJECT_GENOME_H
#define PROJECT_GENOME_H

#include <string>
#include <vector>
#include "Gene.h"
using namespace std;

class Genome {
protected:

    string locus;

    string definition;
public:
    vector<Gene> annotation;

    Genome();

    ~Genome();

    Genome(const string &locus, const string &definition, const vector<Gene> &annotation);

    const vector<Gene> &getAnnotation() const;

    void setAnnotation(vector<Gene> annotation);

    const string &getLocus() const;

    void setLocus(const string &locus);

    const string &getDefinition() const;

    void setDefinition(const string &definition);

};


#endif //PROJECT_GENOME_H
