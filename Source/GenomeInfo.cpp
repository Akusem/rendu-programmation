//
// Created by akusem on 20/10/18.
//

#include "GenomeInfo.h"

GenomeInfo::GenomeInfo(vector<ulong> m_index, pair<uchar*, ulong>& m_seq,
        pair<uchar*, ulong>& m_seqReverse, vector<Genome>& m_annotation) {
    index = m_index;
    sequence = m_seq;
    sequenceReverse = m_seqReverse;
    annotation = m_annotation;
    cadreDeLecture.resize(6);
    tempCadreDeLecture.resize(6);
    indexCadreDeLecture.resize(6);
    sequenceSST = new SSTree((uchar *) "atg", 4);
    sequenceReverseSST = new SSTree((uchar *) "atg", 4);
}
GenomeInfo::~GenomeInfo() {

}