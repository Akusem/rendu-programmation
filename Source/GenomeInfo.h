//
// Created by akusem on 20/10/18.
//

#ifndef PROJECT_GENOMEINFO_H
#define PROJECT_GENOMEINFO_H

#include <vector>
#include "cst_v_1_1/SSTree.h"
#include "Genome.h"
using namespace std;

class GenomeInfo {
public:
    vector<ulong> index;
    pair<uchar*, ulong> sequence;
    pair<uchar*, ulong> sequenceReverse;
    vector<Genome> annotation;
    SSTree *sequenceSST;
    SSTree *sequenceReverseSST;
    vector<string> tempCadreDeLecture;
    vector<pair<uchar*, ulong>> cadreDeLecture;
    vector<SSTree*> cadreDeLectureSST;
    vector<vector<ulong>> indexCadreDeLecture;
    GenomeInfo(vector<ulong> m_index, pair<uchar*, ulong>& m_seq,
               pair<uchar*, ulong>& m_seqReverse, vector<Genome>& m_annotation);
    virtual ~GenomeInfo();
};


#endif //PROJECT_GENOMEINFO_H
