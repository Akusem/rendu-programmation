#include "Objet_biologique.h"

ObjetBiologique::ObjetBiologique() : posDeb(0), posFin(1), name("") {
}

ObjetBiologique::ObjetBiologique(string m_name, long Deb, long Fin) {
    name = m_name;
    posDeb = Deb;
    posFin = Fin;
}

ObjetBiologique::~ObjetBiologique() {}

string ObjetBiologique::getName() {
    return name;
}

pair<long, long> ObjetBiologique::getPosition() const {
    return make_pair(posDeb, posFin);
}

void ObjetBiologique::setName(string m_name) {
    name = m_name;
}
void ObjetBiologique::setPosition(long Deb, long Fin) {
    posDeb = Deb;
    posFin = Fin;
}
