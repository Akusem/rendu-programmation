#ifndef DEF_OBJET_BIOLOGIQUE
#define DEF_OBJET_BIOLOGIQUE

#include <string>
#include <iostream>
using namespace std;

class ObjetBiologique 
{
protected:
    long posDeb;
    long posFin;
    string name;

public:

    ObjetBiologique();
    ObjetBiologique(string m_name, long Deb, long Fin);
    string getName();
    pair<long, long> getPosition() const;
    void setName(string m_name);
    void setPosition(long Deb, long Fin);
    virtual ~ObjetBiologique();
};

#endif