#include "fenetre.h"
#include "Gene.h"
#include "GenomeInfo.h"

// Ce fichier créé un objet Fenetre de la classe Fenetre, et lui associe des
// conteneurs permettant d'afficher toutes les options de notre programme.

Fenetre::Fenetre() :
    Bouton_fichier1("Sélectionner le génome 1"), 
    Bouton_fichier2("Sélectionner le génome 2"),
    nucleotide(Choix_seq, "Séquence nucléotidique"), 
    proteine(Choix_seq, "Séquence protéique"), perso(Choix_algo, "Standard"),
    sstree(Choix_algo, "SSTree")
    // Initialisation de certaines variables
    {

    // Définition de la fenêtre
    set_default_size(900, 600); //Déterminer la taille par défaut de la fenêtre.
    set_border_width(10); //Ajouter une bordure de 10px de la fenêtre.


    // Onglet 1 : Présentation //

        // Ecriture du texte de présentation
    Texte_titre.set_markup("<b><big>\nRecherche de motifs génomiques</big></b>\n\n\n");
    Texte_presentation.set_markup("Ce programme vous permet de rechercher un motif choisi "
    "dans deux génomes et de retourner sa (ou ses) position(s) dans les génomes. "
    "Si le motif est sous forme nucléotidique, il peut être recherché par un algorithme "
    "spécifique, SSTree, qui créé un arbre des suffixes de chaque génome (à utiliser dans "
    "le cas de recherches de motifs multiples). \n\n\n Pour plus d'informations, veuillez "
    "vous référer au README.\n\n");
    
        // Mise en page du texte
    Texte_titre.set_justify(Gtk::JUSTIFY_CENTER); // Centre le texte
    Texte_titre.set_line_wrap(); // Permet au texte d'aller à la ligne
    Texte_presentation.set_justify(Gtk::JUSTIFY_FILL); // Justifie le texte
    Texte_presentation.set_line_wrap();//

        // Ajout d'un texte dans le bouton suivant1
    Bouton_suivant1.set_label("Rechercher un motif");

        // Ajoute le bouton suivant1 à la box navigation1
    Box_navigation1.pack_end(Bouton_suivant1);

        // Ajout widgets à la page 1 (composée d'une box horizontale)
    Box_page1.pack_start(Texte_titre, Gtk::PACK_SHRINK);
    Box_page1.pack_start(Texte_presentation, Gtk::PACK_SHRINK);
    Box_page1.pack_end(Box_navigation1);


    // Onglet 2 : Lecture de fichiers //

        // Texte

    Texte_fichiers.set_markup("Pour commencer, veuillez entrer deux fichiers au format .gbff.\n"
    "<small><i>Vous pouvez télécharger le génome d'un organisme au format .gbff en cliquant "
    "<a href=\'https://www.ncbi.nlm.nih.gov/genome/browse#!/overview/'>ici</a>.</i></small>\n\n");
    Texte_fichiers.set_justify(Gtk::JUSTIFY_FILL);

        // Mise en place des boutons

    Bouton_fichier1.set_can_focus(false);
    Bouton_fichier2.set_can_focus(false);
    Bouton_fichier1.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::selection_fichier1)); // ouvre le fichier 1
    Bouton_fichier2.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::selection_fichier2)); // ouvre le fichier2

    Bouton_fichier1.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::createSstree)); // si un nouveau fichier est choisi,
                        // un nouvel abre SSTree est créé, à condition que 
                        // l'option SSTree soit sélectionnée

    Bouton_fichier1.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::createSstree)); 

    Bouton_precedent2.set_label("Afficher la présentation");
    Bouton_suivant2.set_label("Etape suivante");

        // Ajout des boutons de changement de page à la box_navigation2
    Box_navigation2.pack_start(Bouton_precedent2);
    Box_navigation2.pack_start(Bouton_suivant2);

        // Ajout des boutons dans un grid pour une meilleure mise en page

    Grid_page2.attach(Bouton_fichier1, 1, 0, 1, 1);
    Grid_page2.attach(nom_fichier1, 2, 0, 1, 1);
    Grid_page2.attach(Bouton_fichier2, 1, 3, 1, 1);
    Grid_page2.attach(nom_fichier2, 2, 3, 1, 1);
    Grid_page2.set_row_spacing(20);
    Grid_page2.set_column_spacing(20);

        // Ajout des éléments dans la page 2

    Box_page2.pack_start(Texte_fichiers);
    Box_page2.pack_start(Grid_page2);
    Box_page2.pack_end(Box_navigation2);


    // Onglet 3 : Motifs //

        // Texte à afficher 
    Texte_seq.set_markup("Veuillez entrez dans le champ ci-dessous un"
    "motif à rechercher, en précisant s'il est sous forme nucléotidique"
    "ou protéique.");
    Texte_arbre.set_markup("Vous pouvez retrouver un motif en cherchant"
    "son occurence directement dans le génome, ou en utilisant l'algorithme"
    "SSTree.\n"
    "<small><i> Pour plus de renseignements, veuillez vous référer au README."
    "</i></small>\n\n");

        // Création d'une zone de texte pour écrire le motif
    zone_texte.set_alignment(Gtk::ALIGN_START);

        // Boutons

    Bouton_precedent3.set_label("Etape précédente");
    Bouton_suivant3.set_label("Afficher les résultats");


    Bouton_suivant3.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::run_search)); // lance le programme de recherche de motif
                    // présent dans main.cpp

    Box_navigation3.pack_start(Bouton_precedent3);
    Box_navigation3.pack_start(Bouton_suivant3);


        // Ajout des boutons dans un grid pour une meilleure mise en page
    Texte_motif.set_markup("Entrez votre motif ici :");
    Grid_page3a.attach(nucleotide, 1, 0, 1, 1);
    Grid_page3a.attach(proteine, 1, 2, 1, 1);

    Grid_page3c.attach(Texte_motif, 1, 0, 1, 1);
    Grid_page3c.attach(zone_texte, 1, 2, 1, 1);

    HBox_page3.pack_start(Grid_page3a);
    HBox_page3.pack_start(Grid_page3c);

    Grid_page3b.attach(perso, 1, 0, 1, 1);
    Grid_page3b.attach(sstree, 1, 1, 1, 1);

    // Ces deux boutons permettent appellent la fonction prot_select, qui va
    // déterminer si l'algorithme SSTree est utilisable selon le choix effectué
    nucleotide.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::prot_select));

    proteine.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::prot_select));

        // Ajout des éléments dans la page 3

    Box_page3.pack_start(Texte_seq);
    Box_page3.pack_start(HBox_page3);
    Box_page3.pack_start(Texte_arbre);
    Box_page3.pack_start(Grid_page3b);
    Box_page3.pack_end(Box_navigation3);

    Grid_page3a.set_row_spacing(10);
    Grid_page3c.set_row_spacing(10);


    // Onglet 4 : resultats //

        // Création d'un modèle d'arborescence | créé les colonnes à partir de listes

    refTreeModel1 = Gtk::ListStore::create(Columns);

        // Ajout des colonnes dans le tableau

    Table_genome1.append_column("Gène", Columns.col_gene);
    Table_genome1.append_column("Protein ID", Columns.col_prot_id);
    Table_genome1.append_column("Pos start", Columns.col_pos_start);
    Table_genome1.append_column("Pos end", Columns.col_pos_end);
    Table_genome1.append_column("Sens ADN", Columns.col_dna_direction);
    Table_genome1.append_column("Description", Columns.notes);

        // Création d'un deuxième modèle d'arborescence et ajout des colonnes

    refTreeModel2 = Gtk::ListStore::create(Columns);

    Table_genome2.append_column("Gène", Columns.col_gene);
    Table_genome2.append_column("Protein ID", Columns.col_prot_id);
    Table_genome2.append_column("Pos start", Columns.col_pos_start);
    Table_genome2.append_column("Pos end", Columns.col_pos_end);
    Table_genome2.append_column("Sens ADN", Columns.col_dna_direction);
    Table_genome2.append_column("Description", Columns.notes);

        // Paramètres des deux ScrolledWindow

    defilement1.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    defilement2.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);


    Bouton_precedent4.set_label("Etape précédente");
    Bouton_enregistrer.set_label("Enregistrer les résultats");
    Bouton_quitter.set_label("Quitter");

    Box_navigation4.pack_start(Bouton_precedent4);
    Box_navigation4.pack_start(Bouton_enregistrer);
    Box_navigation4.pack_start(Bouton_quitter);
    
        // Sauvegarde des résultats

    Bouton_precedent4.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::clear_treeview));

    Bouton_enregistrer.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::save_search));

    Bouton_quitter.signal_clicked().connect(sigc::mem_fun(*this,
                &Fenetre::quit));

    Table_genome1.set_model(refTreeModel1); // affiche le modèle
    Table_genome2.set_model(refTreeModel2); // affiche le modèle
    defilement1.add(Table_genome1);
    defilement2.add(Table_genome2);
    Box_page4.pack_start(Texte_res1, Gtk::PACK_SHRINK); // la box ne prend que la place 
    Box_page4.pack_start(defilement1);                       // qui lui est nécessaire
    Box_page4.pack_start(Texte_res2, Gtk::PACK_SHRINK);
    Box_page4.pack_start(defilement2);
    Box_page4.pack_start(Box_navigation4, Gtk::PACK_SHRINK);

        // Ajout des différentes pages

    onglets.append_page(Box_page1, "Notes");
    onglets.append_page(Box_page2, "Lire un fichier");
    onglets.append_page(Box_page3, "Options");
    onglets.append_page(Box_page4, "Résultats");

        // Onglets

    onglets.set_can_focus(false);

        // Connexions entre les pages

    Bouton_suivant1.signal_clicked().connect(sigc::mem_fun(onglets, 
        &Gtk::Notebook::next_page));
    Bouton_precedent2.signal_clicked().connect(sigc::mem_fun(onglets, 
        &Gtk::Notebook::prev_page));
    Bouton_suivant2.signal_clicked().connect(sigc::mem_fun(onglets, 
        &Gtk::Notebook::next_page));
    Bouton_precedent3.signal_clicked().connect(sigc::mem_fun(onglets, 
        &Gtk::Notebook::prev_page));
    Bouton_precedent4.signal_clicked().connect(sigc::mem_fun(onglets, 
        &Gtk::Notebook::prev_page));


        // Mise en page du programme

    Box_page.pack_start(onglets);

        // Affichage du programme

    add(Box_page);

    
    show_all(); //Afficher tous les widgets.
}


void Fenetre::selection_fichier1()
// Ouvre une boite de dialogue permettant de sélectionner un fichier, et enregistre
// son nom dans un fichier temporaire
{
  Gtk::FileChooserDialog dialog("Please choose a file", // ouvre une boite de dialogue
          Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_default_size(900, 600);
  dialog.set_transient_for(*this); // ouvre la boite de dialogue devant la fenetre parente

  //Ajoute les boutons "réponse" au dialog
  dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("Open", Gtk::RESPONSE_OK);

  //Ajoute un filtre de telle sorte que seuls les fichiers .gbf puissent être sélectionnés

  auto filter_text = Gtk::FileFilter::create();
  filter_text->set_name("fichiers gbff");
  filter_text->add_pattern("*.gbff"); // mime -> type de média
  dialog.add_filter(filter_text); // ajoute le filtre au dialogue

  //Affiche le dialog et attend une réponse de l'utilisateur
  int result = dialog.run();

  switch(result)
  {
    case(Gtk::RESPONSE_OK): // si un bon fichier est choisi
    {
    nom_fichier1.set_markup(dialog.get_filename());
    std::ofstream file;
    file.open("/tmp/fichier1.txt");
    fileName1 = dialog.get_filename();
      break;
    }
    case(Gtk::RESPONSE_CANCEL): // si annulation
    {
      break;
    }
  }
}

void Fenetre::selection_fichier2()
// Ouvre une boite de dialogue permettant de sélectionner un fichier, et enregistre
// son nom dans un fichier temporaire
{
  Gtk::FileChooserDialog dialog("Please choose a file", // ouvre une boite de dialogue
          Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_default_size(900, 600);
  dialog.set_transient_for(*this); // ouvre la boite de dialogue devant la fenetre parente

  //Ajoute les boutons "réponse" au dialog
  dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("Open", Gtk::RESPONSE_OK);

  //Ajoute un filtre de telle sorte que seuls les fichiers .gbf puissent être sélectionnés

  auto filter_text = Gtk::FileFilter::create();
  filter_text->set_name("fichiers gbff");
  filter_text->add_pattern("*.gbff"); // mime -> type de média
  dialog.add_filter(filter_text); // ajoute le filtre au dialogue

  //Affiche le dialog et attend une réponse de l'utilisateur
  int result = dialog.run();

  switch(result)
  {
    case(Gtk::RESPONSE_OK): // si un bon fichier est choisi
    {
    nom_fichier2.set_markup(dialog.get_filename());
    std::ofstream file;
    file.open("/tmp/fichier2.txt");
    fileName2 = dialog.get_filename();
      break;
    }
    case(Gtk::RESPONSE_CANCEL): // si annulation
    {
      break;
    }
  }
  
}
/**
 * Modifie le booléen sstree pour rentre impossible la recherche SSTree pour les
 * motifs protéiques (non implantée dans le programme)
 */
void Fenetre::prot_select(){
    if(proteine.get_active()){
        sstree.set_sensitive(FALSE);
    }
    else if(nucleotide.get_active()){
        sstree.set_sensitive(TRUE);
    }
}

/**
 * Modifie le booléen sstree pour ne pas avoir à re-créer un arbre SSTree pour
 * chaque motif cherché
 */
void Fenetre::createSstree(){
    useSstree = false;
}

/**
 * Ferme le programme
 */
void Fenetre::quit(){
    exit(1);
}

/**
 * FVide les tableaux de résultats
 */
void Fenetre::clear_treeview(){
    refTreeModel1->clear();
    refTreeModel2->clear();
}