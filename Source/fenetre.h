#ifndef DEF_FENETRE
#define DEF_FENETRE

// gtk_widget_get_size_request()

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/table.h>
#include <gtkmm/grid.h>
#include <gtkmm/dialog.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/notebook.h>
#include <gtkmm/entry.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/stock.h>

#include <string.h>
#include <iostream>
#include <fstream>
#include "Gene.h"



class Fenetre : public Gtk::Window {
    public:
        Fenetre();
        // Initialisation des fonctions de la classe
        void selection_fichier1();
        void selection_fichier2();
        void prot_select();
        void run_search();
        void save_search();
        void createSstree();
        void clear_treeview();
        void quit();


    protected:
            // Création du TreeModel pour faire le tableau des résultats
        class ModelColumns : public Gtk::TreeModel::ColumnRecord
        {
            public:
            ModelColumns()
            // Ajout des colonnes
            {add(col_gene); add(col_prot_id); add(col_pos_start); add(col_pos_end);
            add(col_dna_direction); add(notes);}
            Gtk::TreeModelColumn<Glib::ustring> col_gene;
            Gtk::TreeModelColumn<std::string> col_prot_id;
            Gtk::TreeModelColumn<long> col_pos_start;
            Gtk::TreeModelColumn<long> col_pos_end;
            Gtk::TreeModelColumn<std::string> col_dna_direction;
            Gtk::TreeModelColumn<std::string> notes;
        };

    private:

        // Initialisation des variables de la classe

            // Boléen
        bool useSstree;

            // String
        std::string fileName1;
        std::string fileName2;

            // Barres de défilement
        Gtk::ScrolledWindow defilement1;
        Gtk::ScrolledWindow defilement2;

            // Textes
        Gtk::Label Texte_titre;
        Gtk::Label Texte_presentation;
        Gtk::Label Texte_fichiers;
        Gtk::Label Texte_seq;
        Gtk::Label Texte_motif;
        Gtk::Label Texte_arbre;
        Gtk::Label nom_fichier1;
        Gtk::Label nom_fichier2;
        Gtk::Label Texte_res1;
        Gtk::Label Texte_res2;

            // Boutons
        Gtk::Button Bouton_suivant1;
        Gtk::Button Bouton_precedent2;
        Gtk::Button Bouton_suivant2;
        Gtk::Button Bouton_precedent3;
        Gtk::Button Bouton_suivant3;
        Gtk::Button Bouton_precedent4;
        Gtk::Button Bouton_enregistrer;
        Gtk::Button Bouton_quitter;
        Gtk::Button Bouton_fichier1;
        Gtk::Button Bouton_fichier2;
        Gtk::RadioButtonGroup Choix_seq;
        Gtk::RadioButton nucleotide, proteine;
        Gtk::RadioButtonGroup Choix_algo;
        Gtk::RadioButton perso, sstree;

            // Box
        Gtk::VBox Box_page;
        Gtk::VBox Box_page1;
        Gtk::VBox Box_page2;
        Gtk::VBox Box_page3;
        Gtk::HBox HBox_page3;
        Gtk::VBox Box_page4;

            // Tableau
        ModelColumns Columns;
        Gtk::TreeView Table_genome1;
        Gtk::TreeView Table_genome2;
        Glib::RefPtr<Gtk::ListStore> refTreeModel1;
        Glib::RefPtr<Gtk::ListStore> refTreeModel2;

            // ButtonBox

        Gtk::HButtonBox Box_navigation1;
        Gtk::HButtonBox Box_navigation2;
        Gtk::HButtonBox Box_navigation3;
        Gtk::HButtonBox Box_navigation4;

            // Grid
        Gtk::Grid Grid_page1;
        Gtk::Grid Grid_page2;
        Gtk::Grid Grid_page3a;
        Gtk::Grid Grid_page3b;
        Gtk::Grid Grid_page3c;
        Gtk::Grid Grid_page4;

            // Zone de texte
        Gtk::Entry zone_texte;

            // Onglets
        Gtk::Notebook onglets;

};

#endif
