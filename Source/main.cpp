//
// Created by akusem on 27/09/18.
//

/** @file main.cpp */

#include <fstream>
#include <vector>
#include <cstring>
#include <algorithm>
#include "cst_v_1_1/SSTree.h"
#include "Gene.h"
#include "Genome.h"
#include "GenomeInfo.h"
#include <gtkmm/main.h>
#include "fenetre.h"

using namespace std;

/**
 * Affiche l'aide
 */
void help(){
    cout << "Recherche de motifs de deux génomes. v.1.0" << endl;
    cout << "Usage en mode graphique : ./Projet" << endl;
    cout << "Usage CLI : ./Projet [Génome 1] [Génome 2] -[n || p] [motif] [fichier ouput] [choix algo]" << endl;
    cout << "Options" << endl;
    cout << "-n" << "\t" << "Motif nucléotidique" << endl;
    cout << "-p" << "\t" << "Motif protéique" << endl;
    cout << "-s" << "\t" << "Utiliser SSTree" << endl;
    cout << "-h" << "\t" << "Afficher l'aide" << endl;
}


/**
 * Lis le fichier fournis et le retourne sous forme de pair<uchar*, ulong>
 * @param fileName Le nom du fichier à lire
 * @return
 */
pair<uchar*, ulong> ReadFile(string const& fileName) {

    ifstream file(fileName, ios::in | ios::binary);
    char *fileContent = NULL;
    pair<uchar*, ulong> toReturn;

    if (file) {
        // Creation d'un tableau de char de bonne taille
        file.seekg(0, ios::end); // deplace le curseur à la fin du fichier
        ulong size = file.tellg();
        fileContent = new char[size]; // adapte la taille du tableau
        file.seekg(0, ios::beg); // redeplace le curseur au début du fichier pour le lire

        /* lis le fichier
        * link: https://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
        * Plus vraiment à jour mais l'un des seul à tester différentes méthodes
        * et à les benchmarker -> c'est théoriquement la méthode la plus rapide
        * façon C++
        */
        file.read(fileContent, size);
        //fileContent[size+1] = '\0';
        //cout << size+1 << endl;
        toReturn = make_pair((uchar*)fileContent, size+1);
    } else {
        cout << "Erreur: impossible de lire le fichier" << endl;
        exit(1);
    }

    file.close();
    return toReturn;
}

void printCharArray(pair<uchar*, ulong> array, ulong pos1, ulong pos2) {

    for (; pos1 <= pos2; pos1++ ) {
        cout << array.first[pos1];

    }
    cout << endl;
}

void printVector(vector<long> vec) {
    for (long i = 0; i < vec.size(); i++) {
        cout << "i: " << i << " value: " << vec[i] << endl;
    }
}

void printVector(vector<ulong> vec) {
    for (long i = 0; i < vec.size(); i++) {
        cout << "Index i: " << i << " " << vec[i] << endl;
    }
}

/**
 * Retourne toutes les positions du motif dans la séquence fournis
 * @param genome une séquence/fichier où sera chercher le motif
 * @param motif Le motif à chercher
 * @return Toutes les positions de hits sous forme de vector<long>
 */
vector<long> SearchAllPosition(pair<uchar*, ulong> const& genome, string& motif)
{
    vector<long> posStart;
    for (long i = 0; i < genome.second; i++) {
        for (int j = 0; j < motif.size();) {
            //cout << motif[j] << " " << genome.first[i+j] << " " << j << endl;
            if (motif[j] == genome.first[i+j]) {
                //cout << "Char find: " << i+j << " j: " << j << endl;
                //cout << endl;
                j++;
                if (j == motif.size()) {
                    posStart.push_back(i);
                    //cout << "Position : " << i << endl;
                }
            }
            else
                break;
        }
    }
    return posStart;
}

/**
 * Return the first position of the motif find, return -1 if not find
 * @param charArray Le fichier ou il faut chercher le motif
 * @param motif Motif à trouver dans la ligne
 * @param posStart Position à partir de laquelle il faut chercher (aka la position de la ligne)
 * @return
 */
long FindPositionInLine(pair<uchar*, ulong> const& charArray, string motif, long posStart = 0) {

    long position = -1;
    bool find = false;
    for (long i = posStart; i <= charArray.second; i++) {
        for (int j = 0; j < motif.size(); j++) {
            if (motif[j] == charArray.first[i + j]) {
                if (j == motif.size()-1) {
                    position = i;
                    find = true;
                    //cout << "Position : " << i << endl;
                }
            }   else if (charArray.first[i + j] == '\n')
                find = true;
        }
        if (find)
            break;
    }

    return position;
}

pair<uchar*, ulong> AppendCharArray(uchar* const& array1, ulong const& length1, uchar* const& array2, ulong const& length2)
{
    //pair<uchar*, ulong*> toReturn;
    //cout << array1;
    ulong newSize = length1+length2;
    uchar* newArray;
    newArray = new uchar[newSize];
    long y = 0;
    for (long i = 0; i < length1; i ++) {
        newArray[y] = array1[i];
        //cout << newArray[y];
        y++;
    }
    //newArray[y] = '\0';
    //y++;
    for (long j = 0; j < length2; j++) {
        newArray[y] = array2[j];
        y++;
    }

    //cout << "newArray: " << newArray[15] ;
    //delete(array1, array2);
    //cout << "after delete" << endl;
    return make_pair(newArray, newSize);
}

/**
 * Fonction servant à extraire les séquences du fichier gbff charger en mémoire.
 * Lis les séquences des différents chromosomes/plasmides et concatène le tout dans un uchar*
 * tout en créant un index permettant de determiner ou se trouve chaque sous-séquence dans la
 * séquences final
 * @param genome Le fichier gbff lus et donné sous forme de pair<uchar*,ulong>
 * @param startPos Un vector<long> contenant toute les positions de ORIGIN dans le fichier
 * @param endPos Un vector<long> contenant toute les positions de fin des séquences dans le fichier
 * @return Retourne l'index en 1er position de la pair, et la séquence concaténé sous forme de
 * pair<uchar*, ulong>
 */
pair<vector<ulong>, pair<uchar*, ulong>> ExtractSeq(pair<uchar*, ulong> genome, vector<long> startPos, vector<long> endPos)
{
    pair<uchar*, ulong> toReturn;
    uchar *saveSeq;
    ulong saveSize, sizeIndex = 0;
    vector<ulong> index = {0};
    //cout << "StarPos size: " << startPos.size() << endl;
    //printVector(startPos);
    for (int y = 0; y < startPos.size(); y++) {
        uchar *seq;
        seq = new uchar[endPos[y] - startPos[y] - 6];

        long j = 0;
        for (long i = startPos[y] + 6; i <= endPos[y]; i++) {
            if (genome.first[i] == 'a' || genome.first[i] == 't' || genome.first[i] == 'g' || genome.first[i] == 'c') {
                seq[j] = genome.first[i];
                j++;
            }
        }

        sizeIndex += j;
        //cout << "sizeIndex: " << sizeIndex << endl;
        if (y == 0 && y != startPos.size()-1) {
            saveSeq = seq;
            saveSize = (ulong)j;
            index.push_back(sizeIndex);
        } else if (y == 0 && y == startPos.size()-1) {
            toReturn = make_pair(seq, (ulong)j);
            index.push_back(sizeIndex);
        } else if (y == 1 && y != startPos.size()-1) {
            toReturn = AppendCharArray(saveSeq, saveSize, seq, (ulong)j);
            index.push_back(sizeIndex);
        } else if (y > 1 && y != startPos.size()) {
            toReturn = AppendCharArray(toReturn.first, toReturn.second, seq, (ulong)j);
            index.push_back(sizeIndex);
        }

        //cout << toReturn.first << endl << toReturn.second << endl;
    }

    return make_pair(index, toReturn);
}

/**
 * Extrait les positions de début et de fin d'un gène dans un fichier Gbff
 * @param genome Fichier Gbff sous forme de pair<uchar*, ulong>
 * @param s La position à partir de laquelle il faut chercher l'information (typiquement le début
 * de la ligne où est stock" l'information
 * @return
 */
pair<long, long> FindPosGene(pair<uchar*, ulong> const& genome, long s = 0)
{
    long i = 0;
    string posDeb, posFin;
    bool deb = true, passOrigin = false;
    while (genome.first[s+i] != '\n') {
        //cout << genome.first[s+i] << endl;
        if (genome.first[s+i] == '0' || genome.first[s+i] == '1' ||
                genome.first[s+i] == '2' || genome.first[s+i] == '3' ||
                genome.first[s+i] == '4' || genome.first[s+i] == '5' ||
                genome.first[s+i] == '6' || genome.first[s+i] == '7' ||
                genome.first[s+i] == '8' || genome.first[s+i] == '9') {
            if (deb && !passOrigin)
                posDeb += genome.first[s+i];
            else if (!deb)
                posFin += genome.first[s+i];

        } else if (genome.first[s+i] == '.') {
            deb = false;
        } else if (genome.first[s+i] == ',') {
            passOrigin = true;
            deb = true;
            posFin = "";
        }
        i++;
    }
    //cout << posDeb << '\t' << posFin << endl;
    return make_pair(stol(posDeb), stol(posFin));
}

string ExtractBetweenQuotes(pair<uchar*, ulong> const& genome, long s)
{
    string toReturn;
    int i = 0, j = 0;
    bool end = false, firstQuote = false;
    // recherche les "
    while (!end) {
        if (genome.first[s+i] == '"') {
            if (firstQuote) {
                end = true;
            } else {
                firstQuote = true;
                j = i+1;
            }
        }
        i++;
    }
    // Adapte le string à la bonne taille pour éviter des réallocations mémoire et le rempli
    toReturn.resize((ulong)i-j-1);
    for (int y = 0; j < i-1; y++, j++) {
        toReturn[y] = genome.first[s+j];
    }

    string trueToReturn;
    for (int i = 0; i < toReturn.size(); i++) {
        if (toReturn[i] == ' ' && toReturn[i+1] != ' ' && toReturn[i+1] != '\n') {
            trueToReturn.push_back(toReturn[i]);
        } else if (toReturn[i] != ' ' && toReturn[i] != '\n') {
            trueToReturn.push_back(toReturn[i]);
        }
    }
    //cout << trueToReturn << endl;

    return trueToReturn;
}


string ExtractUntilEndLine(pair<uchar*, ulong> const& genome, long s)
{
    string toReturn;
    int i = 0;
    bool end = false;
    // Recherche la fin de ligne
    while (!end) {
        if (genome.first[s+i] == '\n')
            end = true;
        i++;
    }
    // Adapte le string à la bonne taille pour éviter des réallocations mémoire et le rempli
    toReturn.resize((ulong)i-1);

    for (int j = 0; j < i-1; j++) {
        toReturn[j] = genome.first[s+j];
    }
    //cout << toReturn << endl;
    return toReturn;
}

/**
 * Recherche toutes les positions du motif dans le SSTree fourni et les retournent
 *  sous forme d'un vector<long>.
 * @param sst
 * @param motif
 * @return
 */
vector<long> SearchAllPosition(SSTree *sst, string motif)
{

    vector<ulong> tempRes;
    auto pattern= (uchar*)motif.c_str();
    ulong l = strlen((char*)pattern);
    ulong v = sst->search(pattern, l);
    int compteur = 0;
    // Fonction implementer dans dans SSTree pour retrouver toute les position
    sst->children(v, tempRes, compteur);
    // Transformation en vector de long
    vector<long> toReturn;
    toReturn.resize(tempRes.size());
    for (int i = 0; i < tempRes.size(); i++) {
        toReturn[i] = (long)tempRes[i];
    }
    return toReturn;
}

void AddDollarForSSTree(uchar*& seq, ulong& l)
{
    seq[l] = (uchar)(char)'$';
}

/**
 * Extrait les positions, les produits, le ProteinId et la Note correspondant au Gene/tRNA
 * @param genome Le fichier Gbff chargé sous forme de pair<uchar*, ulong>
 * @return Un vector d'objet Genome contenant les descriptions des Chromosome/Plasmide
 * et les annoations des gènes correspondant
 */
vector<Genome> ExtractAnnotation(pair<uchar*, ulong> const& genome)
{
    string cds = "     CDS  ", seqRefseq = "sequence:RefSeq:", product = "/product=",
            protein = "/protein_id=", note = "/note=", gene = "     gene            ",
            accession = "VERSION     ", definition = "DEFINITION  ",
            source = "     source          ", tRNA = "    tRNA           ";
    
    vector<long> posSource = SearchAllPosition(genome, source);
    vector<long> posGene = SearchAllPosition(genome, gene);
    vector<long> posCDS = SearchAllPosition(genome, cds);
    vector<long> posRefseq = SearchAllPosition(genome,seqRefseq);
    vector<long> posProduct = SearchAllPosition(genome, product);
    vector<long> posProteinId = SearchAllPosition(genome, protein);
    vector<long> posNote = SearchAllPosition(genome, note);
    vector<long> posAccession = SearchAllPosition(genome, accession);
    vector<long> posDefinition = SearchAllPosition(genome, definition);
    // Créer les objet Genome
    vector<Genome> vecGenome(posAccession.size());
    // Les remplis

    for (int i = 0; i < vecGenome.size(); i++) {
        vecGenome[i].setLocus(ExtractUntilEndLine(genome, posAccession[i]+accession.size()));
        vecGenome[i].setDefinition(ExtractUntilEndLine(genome, posDefinition[i]+definition.size()));
        //cout << vecGenome[i].getLocus() << endl;
        //cout << vecGenome[i].getDefinition() << endl;
    }

    vector<Gene> vecGene(posGene.size());
    int numberGenome = 0;
    long cNote = 0, cProtId = 0, cSource = 0;
    ulong posMax = 0, genomeStart = 0;
    for (long i = 0; i < posGene.size(); i++) {
        // Determine si le gène est codé sur le brin complémentaire ou pas
        long pos = FindPositionInLine(genome, (string)"complement", posGene[i]);
        if (pos == -1) {
            vecGene[i].setDnaDirection('+');
        } else {
            vecGene[i].setDnaDirection('-');
        }

        // Extrait les positions des genes
        pair<long, long> temp = FindPosGene(genome, posGene[i]);
        vecGene[i].setPosition(temp.first, temp.second);

        // Get the product and use it as a name
        vecGene[i].setName(ExtractBetweenQuotes(genome, posProduct[i]));

        // Replace le compteur de Note au bonne endroit en cas de décalage
        if (posSource[cSource] < posNote[cNote] && posGene[i] > posNote[cNote]) {
            cSource++;
            cNote++;
        } else if (posGene[i] > posNote[cNote]) {
            cNote++;
        }
        // Place un le champ note dans l'instance de la classe Gene, ou, Si non présent met un '/'
        if ( posGene[i] < posNote[cNote] && posNote[cNote] < posGene[i+1]) {
            vecGene[i].setNote(ExtractBetweenQuotes(genome, posNote[cNote]));
            cNote++;
        } else {
            vecGene[i].setNote("/");
        }

        // Extract proteinId if present or set it to '/'
        if ( posGene[i] < posProteinId[cProtId] && posProteinId[cProtId] < posGene[i+1]) {
            vecGene[i].setProteinId(ExtractBetweenQuotes(genome, posProteinId[cProtId]));
            cProtId++;
        } else {
            vecGene[i].setProteinId("/");
        }


        // Verifie si on est dans un nouvelle élément du genome
        // Si c'est le cas créer un nouveaux Vector contenant les Gene correspondant
        // et les places dans l'objet Genome
        if (posMax < temp.second && i != posGene.size()-1) {
            //cout << posMax << " " << temp.second << endl;
            posMax = (ulong) temp.second;
        } else if (i == posGene.size()-1) {
            //cout << posMax << " " << temp.second << endl;
            posMax = 0;
            //Creation du vector à placer dans l'objet Genome
            vector<Gene> tempVec(i-genomeStart);
            for (ulong j = 0, z = genomeStart; z < i; j++, z++) {
                tempVec[j] = vecGene[z];
            }
            genomeStart = (ulong)i;
            vecGenome[numberGenome].setAnnotation(tempVec);
            numberGenome++;
        } else {
            //cout << posMax << " " << temp.second << endl;
            posMax = 0;
            //Creation du vector à placer dans l'objet Genome
            vector<Gene> tempVec(i-genomeStart);
            for (ulong j = 0, z = genomeStart; z < i; j++, z++) {
                tempVec[j] = vecGene[z];
            }
            genomeStart = (ulong)i;
            vecGenome[numberGenome].setAnnotation(tempVec);
            numberGenome++;
        }

    }

    return vecGenome;
}

/**
 * Fonction qui à partir des positions de hits sur le brin + et - et d'annotation,
 * retrouve si il y a des Gènes ces positions. Si oui les retournes, sinon retournes un objet Gene
 * préremplis
 * @param pos Liste des positions de hit dans le brin +
 * @param posComp Liste des positions de hit dans le brin -
 * @param genomeAnnotation Un vector d'objet Gene correspondant correspondant à la Séquences d'où
 * viennent les hits
 * @param index Un index de position des sous séquences car les séquences peuvent être concaténé
 * @param tailleMotif Taille du motif d'où viennent les hits dans le cas ou il faut remplir un objet
 * Gene si il n'y en a pas correspondant à la position donnée.
 * @return
 */
vector<Gene> LinkToAnnotation(vector<long>& pos, vector<long>& posComp, vector<Genome>& genomeAnnotation,
        vector<ulong> index, long tailleMotif = 0)
{
    // Initialisation de toReturn et si il y a des positions trouver dans pos et posComp
    // le resize à la bonne taille
    vector<Gene> toReturn;

    // concatenation des 2 vector de position pour pouvoir faire une boucle range dessus
    vector<vector<long>> concat = {pos, posComp};
    // Pour les hit de chaque brin
    char brin = '+';
    for (vector<long>& pos: concat) {
        // Verifie que des hits soient présents sur ce brin avant de lancer les calculs
        if (!pos.empty()) {

            // Pour chaque hits
            int compteur = 0;
            for (long& p: pos) {
                // Determine dans quel Chromosome/Plasmide ce trouve le hit en utilisant l'index
                int whichGenome = 0;
                for (int i = 0; i < index.size()-1; i++) {
                    if (index[i] <= p && index[i+1] > p)
                        whichGenome = i;
                }

                // Récupération des annotations
                vector<Gene> &annotation = genomeAnnotation[whichGenome].annotation;

                bool find = false;
                long posInGenome = p - index[whichGenome];
                for (Gene& gene: annotation) {
                    pair<long, long> tempPos = gene.getPosition();
                    // Determine si le hit ce trouve dans un gène
                    if (tempPos.first <= posInGenome && tempPos.second > posInGenome && gene.getDnaDirection() == brin) {
                        toReturn.push_back(gene);
                        compteur++;
                        find = true;
                    } else if (posInGenome < tempPos.first && posInGenome+tailleMotif > tempPos.second && gene.getDnaDirection() == brin) {
                        toReturn.push_back(gene);
                        compteur++;
                        find = true;
                    } else if (posInGenome > tempPos.first && posInGenome < tempPos.second && posInGenome+tailleMotif > tempPos.second) {
                        toReturn.push_back(gene);
                        compteur++;
                        find = true;
                    }
                }
                // si la position du hit ne correspond à aucun gène remplis une instance de
                // la classe Gene vide avec le nom 'No gene in this position'
                if (!find) {
                    Gene temp = Gene();
                    temp.setName("No gene in this position");
                    temp.setPosition(posInGenome, posInGenome+tailleMotif);
                    temp.setDnaDirection(brin);
                    toReturn.push_back(temp);
                    compteur++;
                }
            }
        }
        brin = '-';
    }
 /*
    for (Gene& Annotation: toReturn) {
        cout << Annotation.getPosition().first << " " << Annotation.getPosition().second << endl;
        cout << Annotation.getName() << endl;
        cout << Annotation.getNote() << endl;
        cout << Annotation.getProteinId() << endl;
        cout << Annotation.getDnaDirection() << endl;
    }
 */

    return toReturn;
}


/**
 * Créer le brin complémentaire d'une sequence nucléotidiques tout en prenant en compte des
 * différentes sous séquences contenue dans la séquence concaténé fournis en entrée en utilisant
 * l'index.
 * @param seq Une séquence nucléotidiques (pouvant représenté différentes séquences concaténé)
 * @param index Un index indiquant les positions de début de chaque sous séquences
 * (ainsi que la position de fin)
 * @return Le Brin complémentaire de la séquence fournis
 */
pair<uchar*, ulong> MakeReverse (pair<uchar*, ulong>& seq, vector<ulong>& index){
    uchar *seq_r, *complementary;
    complementary = new uchar[seq.second];
    seq_r = new uchar[seq.second];

    // Creation du reverse en 3' 5'
    for (long i = 0; i <= seq.second; i++) {
        if (seq.first[i] == 'a') {
            complementary[i] = 't';
        } else if (seq.first[i] == 't') {
            complementary[i] = 'a';
        } else if (seq.first[i] == 'g') {
            complementary[i] = 'c';
        } else if (seq.first[i] == 'c') {
            complementary[i] = 'g';
        }
    }

    // Reverse chaque sequence de Chromosome/Plasmide sur place pour qu'il garde le même ordre
    // par rapport à l'index
    long y = 0;
    //cout << index.size() << endl;
    for (int i = 0; i < index.size()-1; i++) {
        //cout << "i: "  << i << endl;
        //cout << "index[i]: " << index[i] << " index[i+1]: " << index[i+1] << endl;
        for (long j = (long)index[i+1]-1; j >= (long)index[i]; j--) {
            //cout << "j: " << j << " y: " << y << endl;
            seq_r[y] = complementary[j];
            y++;
        }
    }
    seq_r[y] = ' ';
    delete complementary;

    return  make_pair(seq_r, seq.second);
}

/**
 * Fonctions agrégratirices, Prend un nom de fichier gbff, en extrait les séquences,
 * les annotations, créer le brin reverse et retourne un objet GenomInfo
 * @param fileName Nom d'un fichier *.gbff à ouvrir
 * @return Un Objet GenomeInfo contenant toute les informations.
 */
GenomeInfo* ExtractAllInfosFromGbff(string fileName)
{
    // Lis le fichier gbff
    pair<uchar*, ulong> gbff = ReadFile(fileName);

    // Trouve toute les position start et end des sequences
    string origin = "ORIGIN      ", end = "\n//";
    vector<long> posOrigin = SearchAllPosition(gbff, origin);
    vector<long> endSeq = SearchAllPosition(gbff, end);

    // Extrait les sequences et les concatènes dans un tableau de uchar, un index est créé pour savoir
    // où commence chaque chromosome/plasmide
    pair<vector<ulong>, pair<uchar*, ulong>> tempSeq = ExtractSeq(gbff, posOrigin, endSeq);
   // Creation du brin reverse
    pair<uchar*, ulong> seqReverse = MakeReverse(tempSeq.second, tempSeq.first);
    // Extrait les annotations du fichier
    vector<Genome> annotation = ExtractAnnotation(gbff);

    GenomeInfo *toReturn = new GenomeInfo(tempSeq.first, tempSeq.second, seqReverse, annotation);
    return toReturn;
}

/**
 *  Retourne le charactère de l'acide aminées correspondant au codons fournis
 * @param c un codons
 * @return
 */
char findAA(string c)
{
    if (c=="tta" || c=="ttg" || c=="cta" || c=="ctg" || c=="ctc" || c=="ctt") {
        return 'L';
    }
    else if (c=="aga" || c=="agg" || c=="cga" || c=="cgg" || c=="cgc" || c=="cgt") {
        return 'R';
    }
    else if (c=="tca" || c=="tct" || c=="tcc" || c=="tcg" || c=="agt" || c=="agc") {
        return 'S';
    }
    else if (c == "gca" || c == "gct" || c == "gcg" || c=="gcc") {
        return 'A';
    }
    else if (c=="aca" || c=="act" || c=="acc" || c=="acg") {
        return 'T';
    }
    else if (c=="gta" || c=="gtt" || c=="gtg" || c=="gtc") {
        return  'V';
    }
    else if (c=="cca" || c=="cct" || c=="ccc" || c=="ccg") {
        return 'P';
    }
    else if (c=="gga" || c=="ggt" || c=="ggc" || c=="ggg") {
        return 'G';
    }
    else if (c=="ata" || c=="att" || c=="atc") {
        return 'I';
    }
    else if (c=="taa" || c=="tag" || c=="tga") {
        return '*';
    }
    else if (c=="tgt" || c=="tgc") {
        return 'C';
    }
    else if (c=="gat" || c=="gac") {
        return 'D';
    }
    else if (c=="gaa" || c=="gag") {
        return 'E';
    }
    else if (c=="ttt" || c=="ttc") {
        return 'F';
    }
    else if (c=="cat" || c=="cac") {
        return 'H';
    }
    else if (c=="aaa" || c=="aag") {
        return 'K';
    }
    else if (c=="aat" || c=="aac") {
        return 'N';
    }
    else if (c=="caa" || c=="cag") {
        return 'Q';
    }
    else if (c=="tat" || c=="tac") {
        return 'Y';
    }
    else if (c=="atg") {
        return 'M';
    }
    else if (c=="tgg") {
        return 'W';
    }
}

/**
 * Fonction qui recherche un motif dans la séquence nucléotidiques de l'objet GenomeInfo
 * et retourne un vector d'objet Gene correspondant au position de hit dans celle-ci.
 * @param genome un objet GenomeInfo dont les attributs sequence et sequenceReverse sont remplis
 * ainsi que l'attribut index
 * @param motif Le motif nucléotidiques à chercher
 * @param sstree Si il faut utilisé SSTree ou pas, si oui la fonciton MakeSSTree doit avoir
 * été executé sur GenomeInfo avant d'utiliser cette fonction
 * @return
 */
vector<Gene> SearchNtInGenome(GenomeInfo* genome, string motif, bool sstree = false)
{
    vector<long> motifPos;
    vector<long> motifPosReverse;
    if (!sstree) {
        // Recherche les positions du motif dans les 2 brins
        motifPos = SearchAllPosition(genome->sequence, motif);
        motifPosReverse = SearchAllPosition(genome->sequenceReverse, motif);
    } else {
        motifPos = SearchAllPosition(genome->sequenceSST, motif);
        motifPosReverse = SearchAllPosition(genome->sequenceReverseSST, motif);
    }
    // Regarde si ces positions corresponde à des gènes
    return LinkToAnnotation(motifPos, motifPosReverse, genome->annotation, genome->index, motif.size());
}

/**
 * Fait la traduction sur les 6 cadres de lectures et la retourne sous forme de vector de string
 * l'attribut indexCadreDeLecture du GenomeInfo fournis stock les index correspondant à la traduction
 * Il faudra faire la traduction en vector<pair<uchar*, ulong>> pour stocké le tout dans le
 * GenomeInfo dans le main. Pk ? PARCE QUE C++
 * @param genome Un objet GenomeInfo dont les attribut sequence et sequenceReverse sont remplis
 * @return
 */
vector<string> Traduction(GenomeInfo* genome)
{
    string key;
    vector<string> tempTrad;
    tempTrad.resize(6);
    vector<ulong> &index = genome->index;
    vector<int> deb = {0,1,2,0,1,2};

    for (int i = 0; i < 6; i++) {
        // En fonction du numero de cadre de lecture utilise une reference sur le brin + ou -
        pair<uchar*, ulong> &seq = (i < 3) ? genome->sequence : genome->sequenceReverse;

        int indexCompt = 1;
        long y = 0;
        // Initialisation des cadres de lectures et des l'index
        tempTrad[i].resize(seq.second/3+genome->index.size());
        genome->indexCadreDeLecture[i].resize(index.size());
        genome->indexCadreDeLecture[i][0] = 0;
        for (long j = deb[i]; j <= seq.second+3; j += 3) {
            if (index[indexCompt]-1 < j+2 && indexCompt <= index.size()) {
                // Note l'index de la nouvelle sequence
                j = (long)index[indexCompt]+(long)deb[i]-3;
                genome->indexCadreDeLecture[i][indexCompt] = (ulong)y;
                indexCompt++;
            } else if (j <= seq.second && indexCompt <= index.size()) {
                // Fait la traduction
                key = seq.first[j];
                key += seq.first[j+1];
                key += seq.first[j+2];
                //tempTrad[i][y] = code[key];
                tempTrad[i][y] = findAA(key);
                y++;
            }
            // Dans le cas l'itérateur dépasse la taille de la séquence, réduit y pour
            // éviter d'avoir une case vide dans le string
            if (j == seq.second) {
                y--;
            }
        }
        // Ajout le signe $ à la fin pour SSTree
        tempTrad[i][y] = '$';
        y++;
        // Adapte la taille
        tempTrad[i].resize((ulong)y);
    }
    return tempTrad;
}

/**
 * Ajoute le dollar à la fin de la sequence et de la sequence complémentaire et créer les
 * 2 SSTree correspondant qui sont stocké dans l'objet GenomeInfo fournis en entrée
 * @param genome Un objet GenomeInfo donc les attribut sequence et sequenceReverse sont remplis
 */
void MakeSSTree(GenomeInfo* genome)
{
    AddDollarForSSTree(genome->sequenceReverse.first, genome->sequenceReverse.second);
    AddDollarForSSTree(genome->sequence.first, genome->sequence.second);
    genome->sequenceSST = new SSTree(genome->sequence.first, strlen((char*)genome->sequence.first)+1);
    genome->sequenceReverseSST = new SSTree(genome->sequenceReverse.first, strlen((char*)genome->sequenceReverse.first)+1);
}

/**
 * Recherche un motif protéiques dans les 6 cadres de lectures du GenomeInfo fournis et
 * fait le lien avec les annotations des gènes ce trouvant au positions trouvées.
 * /!\SSTree ne fonctionne pas
 * @param genome Un objet GenomeInfo étant passé par la fonction Traduction précédemment
 * @param motif Le motif protéiques à chercher
 * @param sstree Si il faut utiliser ou pas SSTree (/!\ SStree non fonctionnel)
 * @return Un vector d'objet Gene correspondant au positions trouvées
 */
vector<Gene> SearchAA(GenomeInfo* genome, string motif, bool sstree = false) {
    int deb[6] = {0,1,2,0,1,2};
    vector<long> hitNt, hitReverseNt;
    vector<vector<long>> hit;
    hit.resize(6);

    // Find the motif in the proteins
    if (!sstree) {
        for (int i = 0; i < 6; i++) {
            hit[i] = SearchAllPosition(genome->cadreDeLecture[i], motif);
        }
    } else {
        for (int i = 0; i < 6; i++) {
            hit[i] = SearchAllPosition(genome->cadreDeLectureSST[i], motif);
        }
    }

    vector<vector<ulong>> &index = genome->indexCadreDeLecture;
    // Transfert en position nucleotidique
    for (int i = 0; i < 6; i++) {
        // Verifie qu'il y ait des hit
        if (!hit[i].empty()) {
            // Pour chaque hit
            for (long& h: hit[i]) {
                // Determine le Plasmide/Chromosome correspondant
                int whichGenome = 0;
                for (int j = 0; j < index[i].size(); j++) {
                    if (index[i][j] <= h && index[i][j+1] > h)
                        whichGenome = j;
                }
                // Transfert de la position en AA à la position sur la sequence de NT concaténée
                long posInGenomeAA = h-index[i][whichGenome];
                long posInNt = posInGenomeAA*3+deb[i];
                if (i < 3)
                    hitNt.push_back(genome->index[whichGenome]+posInNt);
                else
                    hitReverseNt.push_back(genome->index[whichGenome]+posInNt);
            }
        }
    }


    vector<Gene> toReturn = LinkToAnnotation(hitNt, hitReverseNt, genome->annotation, genome->index, motif.size());
    return toReturn;
}

/**
 * NOT WORKING
 * @param genome
 * @return
 */
vector<SSTree*> MakeSSTForAA(GenomeInfo& genome) {
    SSTree *c0 = new SSTree(genome.cadreDeLecture[0].first, genome.cadreDeLecture[0].second);
    SSTree *c1 = new SSTree(genome.cadreDeLecture[1].first, genome.cadreDeLecture[2].second);
    vector<SSTree*> toReturn = {c0, c1};
    return toReturn;
}

/**
 * Prend un string et le met en majuscule
 * @param motif
 */
void Upper(string& motif) {
    transform(motif.begin(), motif.end(),motif.begin(), ::toupper);
}

/**
 * Prend un string et le met en minuscule
 * @param motif
 */
void Lower(string& motif) {
    transform(motif.begin(), motif.end(),motif.begin(), ::tolower);
}

/**
 * Prend un motif et verifie qu'il soit bien nucléotidique, le met en minuscule au passage
 * @param motif Motif à vérifier
 * @return
 */
bool IsNt(string& motif) {
    Lower(motif);
    bool isNt = true;
    for (char& nt: motif) {
        if (nt != 'a' && nt != 't' && nt != 'g' && nt != 'c')
            isNt = false;
    }
    return isNt;
}

/**
 * Prend un motif et verifie qu'il soit bien nucléotidique, le met en majucules au passage
 * @param motif
 * @return
 */
bool IsAA(string& motif) {
    Upper(motif);
    bool isAA = true;
    for (char &AA: motif) {
        if (AA != '*' && AA != 'A' && AA != 'R' && AA != 'D' &&
            AA != 'N' && AA != 'C' && AA != 'E' && AA != 'Q' &&
            AA != 'G' && AA != 'H' && AA != 'I' && AA != 'L' &&
            AA != 'K' && AA != 'M' && AA != 'F' && AA != 'P' &&
            AA != 'S' && AA != 'T' && AA != 'W' && AA != 'Y' &&
            AA != 'V') {
            isAA = false;
        }
    }
    return isAA;
}

/**
 * Cette fonction appartient à la classe fenêtre. Elle permet de lancer le programme
 * à partir des options sélectionnées dans l'interface graphique.
 */
void Fenetre::run_search()
{
    // Extraction des annotations des deux génomes
    GenomeInfo* genome1 = ExtractAllInfosFromGbff(fileName1);
    GenomeInfo* genome2 = ExtractAllInfosFromGbff(fileName2);
    // Récupération du motif
    string motif = zone_texte.get_text();
    // Création des vecteurs de résultats
    vector<Gene> annotation1;
    vector<Gene> annotation2;
    // Si le motif est nucléotidique
    if(nucleotide.get_active()){
        // Cherche si le motif est bien nucléotidique
        if (IsNt(motif) == false){
            Gtk::MessageDialog dialogue(*this, "Veuillez entrer un motif nucléotidique.", true); 
            dialogue.run();
            // Si ce n'est pas le cas, affiche un avertissement et n'exécute pas le programme
            return;
        };
        if (perso.get_active()){
            // Si la recherche standard est activée
            annotation1 = SearchNtInGenome(genome1, motif);
            annotation2 = SearchNtInGenome(genome2, motif);
        }
        else if (sstree.get_active()){
            // Si la recherche avec SSTree est activée
            if (useSstree == false){
                // Si l'arbre SSTree n'est pas créé
                MakeSSTree(genome1);
                MakeSSTree(genome2);
                useSstree = true;
            }
            annotation1 = SearchNtInGenome(genome1, motif, true); // ajout de l'option true quand sstree est activé
            annotation2 = SearchNtInGenome(genome2, motif, true);
        }
    }
    else if(proteine.get_active()){
        // Si le motif est protéique
        bool test = IsAA(motif);
        if (IsAA(motif) == false){
            // Si ce n'est pas le cas, affiche un avertissement et n'exécute pas le programme
            Gtk::MessageDialog dialogue2(*this, "Veuillez entrer un motif protéique.", true); 
            dialogue2.run();
            return;
        };
        vector<string> res1 = Traduction(genome1);
        vector<string> res2 = Traduction(genome2);
        genome1->cadreDeLecture.resize(6);
        for (int i = 0; i < 6; i++) {
            genome1->cadreDeLecture[i] = make_pair((uchar*)res1[i].c_str(), res1[i].size()+2);
        }
        genome2->cadreDeLecture.resize(6);
        for (int i = 0; i < 6; i++) {
            genome2->cadreDeLecture[i] = make_pair((uchar*)res2[i].c_str(), res2[i].size()+2);
        }
        vector<Gene> annotation1 = SearchAA(genome1, motif);
        vector<Gene> annotation2 = SearchAA(genome2, motif);
    }
    string fileName =  "\n<b>Génome 1 :</b>" + fileName1 + "\n"; 
    // Création de la string qui s'affichera en haut du tableau (nom du fichier)
    Texte_res1.set_markup(fileName);
    // Récupère le nom du fichier
    ofstream file_annotation1;
    file_annotation1.open("/tmp/file_annotation1.txt");
    // Création d'un fichier temporaire qui récupère les informations du génome 1
    for (Gene& Annotation: annotation1) {
        // Ajout des résultats dans le tableau de résultats
        Gtk::TreeModel::Row row = *(refTreeModel1->append());
        row[Columns.col_gene] = Annotation.getName();
        row[Columns.col_prot_id] = Annotation.getProteinId();
        row[Columns.col_pos_start] = Annotation.getPosition().first;
        row[Columns.col_pos_end] = Annotation.getPosition().second;
        int sens = Annotation.getDnaDirection();
        // Conversion du code ASCII en string
        if (sens == 43){
            row[Columns.col_dna_direction] = "+";
        }
        else if (sens == 45){
            row[Columns.col_dna_direction] = "-";
        }
        row[Columns.notes] = Annotation.getNote();
        file_annotation1 << Annotation.getName() << "\t" << Annotation.getProteinId() << "\t" << 
        Annotation.getPosition().first << "\t" << Annotation.getPosition().second <<
        "\t" << Annotation.getDnaDirection() << "\t" << Annotation.getNote() << endl;
        // écriture des résultats dans le fichier temporaire
    }
    file_annotation1.close();

    // Même fonctionnement que pour les résultats du génome 1
    fileName = "\n<b>Génome 2 :</b> " + fileName2 + "\n"; 
    Texte_res2.set_markup(fileName);
    ofstream file_annotation2;
    file_annotation2.open("/tmp/file_annotation2.txt");
    for (Gene& Annotation: annotation2) {
        Gtk::TreeModel::Row row = *(refTreeModel2->append());
        row[Columns.col_gene] = Annotation.getName();
        row[Columns.col_prot_id] = Annotation.getProteinId();
        row[Columns.col_pos_start] = Annotation.getPosition().first;
        row[Columns.col_pos_end] = Annotation.getPosition().second;
        row[Columns.col_pos_end] = Annotation.getPosition().second;
        int sens = Annotation.getDnaDirection();
        if (sens == 43){
            row[Columns.col_dna_direction] = "+";
        }
        else if (sens == 45){
            row[Columns.col_dna_direction] = "-";
        }
        row[Columns.notes] = Annotation.getNote();
        file_annotation2 << Annotation.getName() << "\t" << Annotation.getProteinId() << "\t" << 
        Annotation.getPosition().first << "\t" << Annotation.getPosition().second <<
        "\t" << Annotation.getDnaDirection() << "\t" << Annotation.getNote() << endl;
    }
    file_annotation1.close();
    onglets.next_page();
}

/**
 * Cette fonction appartient à la classe fenêtre. Elle permet d'écrire
 * les résultats dans un nouveau fichier choisi par l'utilisateur
 */

void Fenetre::save_search(){
  Gtk::FileChooserDialog dialog("Please choose a file", // ouvre une boite de dialogue
          Gtk::FILE_CHOOSER_ACTION_SAVE); //_SAVE pour sauvegarder fichier
    dialog.set_default_size(900, 600);
  dialog.set_transient_for(*this); // ouvre la boite de dialogue devant la fenetre parente

  //Ajoute les boutons "réponse" au dialog
  dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
  dialog.add_button("Save", Gtk::RESPONSE_OK);

  int result = dialog.run();

  //Handle the response:
  switch(result)
  {
    case(Gtk::RESPONSE_OK): // si un bon fichier est choisi
    {
    string fichier_res = dialog.get_filename();
    ofstream file_result;
    file_result.open(fichier_res);
    string fileContent; 
    ifstream file1("/tmp/file_annotation1.txt", ios::in | ios::binary);
    // Lecture du fichier temporaire comportant les résultats pour le génome 1
    if (file1) {
        // Si le fichier est lisible
        file1.seekg(0, ios::end); // Déplace le curseur à la fin du fichier
        fileContent.resize(file1.tellg()); // Adapte la taille du string pour éviter d'avoir à bouger le string dans la mémoire
        file1.seekg(0, ios::beg); // Redéplace le curseur au début du fichier pour le lire
        file1.read(&fileContent[0], fileContent.size());
    }
    // Ecriture du nom des colonnes dans le fichier de résultat
    file_result << "nomGene" << "\t" << "protID" << "\t" << "start" << "\t" << 
        "end" << "\t" << "sensADN" << "\t" << "Description" << endl;
    file_result << "Genome 1" << endl;
    file_result << fileContent << endl;
    fileContent = "";
    ifstream file2("/tmp/file_annotation2.txt", ios::in | ios::binary);
    // Même opération pour le fichier génome 2
    if (file2) {
        // Si le fichier est lisible
        file2.seekg(0, ios::end); // Déplace le curseur à la fin du fichier
        fileContent.resize(file2.tellg()); // Adapte la taille du string pour éviter d'avoir à bouger le string dans la mémoire
        file2.seekg(0, ios::beg); // Redéplace le curseur au début du fichier pour le lire
        file2.read(&fileContent[0], fileContent.size());
    }
    file_result << "Genome 2" << endl;
    file_result << fileContent << endl;
    file_result.close();
      break;
    }
    case(Gtk::RESPONSE_CANCEL): // si annulation
    {
      break;
    }
  }
}

int main(int argc, char* argv[])
{
    if (argc == 1){
        Gtk::Main app(argc, argv);
        Fenetre fenetre;
        Gtk::Main::run(fenetre);
    }
    else {
        vector<string> arg;
        for (int i = 1; i < argc; i++){
        // Création d'un vecteur de string arg contenant tous les arguments renseignés
        // par l'utilisateur
            arg.push_back(argv[i]);
        }
        if (find(arg.begin(), arg.end(), "-h") != arg.end()){
            // affiche l'aide
            help();
            return 0;
        }
        // Même fonctionnement que Fenetre::run_search :
        vector<Gene> annotation1, annotation2;
        GenomeInfo* genome1 = ExtractAllInfosFromGbff(argv[1]);
        GenomeInfo* genome2 = ExtractAllInfosFromGbff(argv[2]); 
        // extraient les infos des génomes fournis par l'utilisateur
        string motif = argv[4]; // récupère le motif donné par l'utilisateur
        if (find(arg.begin(), arg.end(), "-n") != arg.end()){
            // Si le motif est nucléotidique
            if (IsNt(motif) == false){
                // SI le motif n'est pas nucléotidique, message d'information et fin du programme
                cout << "Veuillez entrer un motif nucléotidique." << endl;
                return 0;
            };
            if (find(arg.begin(), arg.end(), "-s") != arg.end()){
                // Si SSTree est activé
                MakeSSTree(genome1);
                MakeSSTree(genome2);
                annotation1 = SearchNtInGenome(genome1, motif, true);
                annotation2 = SearchNtInGenome(genome2, motif, true);
            }
            else {
                // Recherche standard
            annotation1 = SearchNtInGenome(genome1, motif);
            annotation2 = SearchNtInGenome(genome2, motif);         
            }
        }
        else if (find(arg.begin(), arg.end(), "-p") != arg.end()){
            // Si le motif est protéique
            if (IsAA(motif) == false){
                // SI ce n'est pas le cas, message d'information et fin du programme
                cout << "Veuillez entrer un motif protéique." << endl;
                return 0;
            };
            vector<string> res1 = Traduction(genome1);
            vector<string> res2 = Traduction(genome2);
            genome1->cadreDeLecture.resize(6);
            for (int i = 0; i < 6; i++) {
                genome1->cadreDeLecture[i] = make_pair((uchar*)res1[i].c_str(), res1[i].size()+2);
            }
            genome2->cadreDeLecture.resize(6);
            for (int i = 0; i < 6; i++) {
                genome2->cadreDeLecture[i] = make_pair((uchar*)res2[i].c_str(), res2[i].size()+2);
            }
            annotation1 = SearchAA(genome1, motif);
            annotation2 = SearchAA(genome2, motif);
        }
        ofstream file;
        file.open(argv[5]);
        // Ecriture dans le fichier de résultat renseigné par l'utilisateur
        file << "nomGene" << "\t" << "protID" << "\t" << "start" << "\t" << 
        "end" << "\t" << "sensADN" << "\t" << "Description" << endl;
        file << "Genome 1" << endl;
        for (Gene& Annotation: annotation1){
            file << Annotation.getName() << "\t" << Annotation.getProteinId() << "\t" << 
            Annotation.getPosition().first << "\t" << Annotation.getPosition().second <<
            "\t" << Annotation.getDnaDirection() << "\t" << Annotation.getNote() << endl;
        }  
        file << "Genome 2" << endl;
        for (Gene& Annotation: annotation2){
            file << Annotation.getName() << "\t" << Annotation.getProteinId() << "\t" << 
            Annotation.getPosition().first << "\t" << Annotation.getPosition().second <<
            "\t" << Annotation.getDnaDirection() << "\t" << Annotation.getNote() << endl;
        }  
        file.close(); 
    }
    return 0;
}
